 import processing.net.*;
import controlP5.*;

ControlP5 cp5;
Client myClient;

Knob ControlKnob;

Textarea myTextarea;
Println console;

PFont font;//font object
PFont controlFont;
PFont controlFont2;
PFont controlFont3;
void setup() {
  
  size(400,400);
  smooth();
  noStroke();
  //background(51);
  
  cp5 = new ControlP5(this);
  font = createFont("calibri light", 30);
  controlFont = createFont("calibri light", 20);
  controlFont2 = createFont("calibri light", 16);
  controlFont3 = createFont("calibri light", 12);
  myClient = new Client(this, "192.168.1.16", 80);//
  
 ControlKnob = cp5.addKnob("DriveControl")
               .setRange(0,255)
               .setValue(0)
               .setColorForeground(color(200,0,0))
               .setColorBackground(color(255,0,0))
               .setColorActive(color(0,225,0))
               .setPosition(100,100)
               .setRadius(100)
               .setDragDirection(Knob.HORIZONTAL)
               .setFont(controlFont)
               ;
               
 myTextarea = cp5.addTextarea("Processing Output")
                .setPosition(143,353)
                .setSize(110,25)
                .setFont(createFont("arial",12))
                .setLineHeight(10)
                .setColorBackground(color(255,255,255))
                .setColor(color(0))
                ;
  
  cp5.addButton("TURN_180")
     //.setValue('X')
     .setPosition(20, 180)
     .setSize(60,30)
     ;
     
  
  //displaying the data beneath

  console = cp5.addConsole(myTextarea);   
  background(51);

}

void draw() {
  font = createFont("calibri light", 30); 
  
   textFont(font);
   fill(color(255,255,255));
   text("2E10: Group Y07", 95, 45);
   text("Arduino Buggy Controls", 55, 75);
  
   fill(color(255,255,255));
   textFont(controlFont2);
   text("Obstacle Detected: ", 138, 345);
   
   fill(color(255,255,255));
   textFont(controlFont2);
   text("kp = 1 / 7.3", 300, 245);
   text("ki = 1 / 20" , 300, 265);
   text("kd = 2" , 300, 285);
   
   fill(color(255,255,255));
   textFont(controlFont2);
   text("Forward", 30, 165);
   text("Backward", 310, 165);
   //meter
    
   
    char data = myClient.readChar();
    
    switch(data){
      case('5'):
        println("Stop: 15cm");
        break;
      case('1'):
        println("Clear");
        break;
    }
     
  Triangle(); 
}
   
void DriveControl(int Value)
{
  if (myClient.active())
  {
      myClient.write(Value);
      if(Value == 0){
      }
   }
}
void Triangle()
{
  
   color triColourG = color(0,255,0);
   color triColourR = color(255,0,0);
  
   if (myClient.active())
     {
      char x = myClient.readChar();
       if(myClient.active())
       {
         if( x == 'f')
         {
          fill(triColourG);
          triangle(30, 145, 58, 90, 86, 145);//left
          
          fill(triColourR);
          triangle(310, 90, 338, 145, 366, 90);//right
          }
          else if( x == 'b')
          {
            fill(triColourR);
            triangle(30, 145, 58, 90, 86, 145);//left
            
            fill(triColourG);
            triangle(310, 90, 338, 145, 366, 90);//right
          }
        }
      }
}

void TURN_180()
{
  if(myClient.active()){
    myClient.write('X');
  }
}

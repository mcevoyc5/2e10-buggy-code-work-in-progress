#include <WiFiNINA.h>//connect server/client

char ssid[] = "eircom04275807";//wifi info
char pass[] = "e8bd6eaa185f";
WiFiServer server(80);//server address
//int high = 230;
//int forward_speed = 180;
int no_speed = 0;

//motor pins
const int right_forward = 5;//L1
const int right_backward= 6;//L2
const int left_backward = 12;//R1
const int left_forward = 2;//r2

//IR Control
const int LED_PIN = 13;
const int LEYE = 3;
const int REYE = 11;

//speed pins
//const int Enable_L = 17;
//const int Enable_R = 16;

//ultrasonic
const int trigPin = 10;
const int echoPin = 9;

//ultrasonic eqn
float duration;
int distance;//convenience for outputting
//PID
double kp = (1/7.3);
double ki = 1/20;
double kd = 2;
 
unsigned long currentTime, previousTime;
double elapsedTime;
double error;
double lastError;
const double setPoint = 15;
double cumError, rateError;
double Speedvalue;

//outputting to GUI
char state;
char prevState = 'z';

bool sentinal;

void setup() {
  Serial.begin(9600);
  
  //server/client connection
  WiFi.begin(ssid, pass);
  IPAddress ip = WiFi.localIP();
  
  //confirm connection
  Serial.print("IP Address:");
  Serial.println(ip);// prints when ip is connected
  server.begin();//starts the server
  
  //set-up ultra
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  //set up motor pins
  pinMode(left_forward, OUTPUT );
  pinMode( left_backward, OUTPUT );
  pinMode( right_forward, OUTPUT );
  pinMode(right_backward, OUTPUT );
  //ir set-up
  pinMode( REYE, INPUT);
  pinMode( LEYE, INPUT);
  pinMode (LED_PIN, OUTPUT);//sort of superfluous


  //function for printing distance
  attachInterrupt(digitalPinToInterrupt(echoPin), ISR, CHANGE);//for printing

}

void loop() {//trying high low speeds to make pins work
  //wifi object
  WiFiClient client = server.available();
  sentinal = 0;
  //checking connection
  if (client.connected()){
   Serial.println("Client Connected");
    int c = 1;// = client.read();
    char turn = client.read();
    while(c != 0 && turn != 'X'){
      c = client.read();//controls on off
      //server.write(1);//find out what this does
      
      digitalWrite(trigPin, HIGH);
      delayMicroseconds(10);
      digitalWrite(trigPin, LOW);
      
      duration = pulseIn(echoPin, HIGH);
      distance = (duration*.0293)/2;
      
      Speedvalue = computePID(distance);
      drive(distance, Speedvalue);//actual speed value is computed in drive functio
    }
     
     if(turn == 'X' )
     {
      while (sentinal != 1){
      
      p180TURN();
      delay(1200);       
      STOP();
      sentinal = 1;
      }
     }
      
  }
     
  STOP();
}

double computePID(double inp){     
    currentTime = millis();                //get current time
    elapsedTime = (double)(currentTime - previousTime);        //compute time elapsed from previous computation
    
    error = (setPoint - inp);                                // determine error
    cumError += error * elapsedTime;                // compute integral
    rateError = (error - lastError)/elapsedTime;   // compute derivative

    double out = kp*error + ki*cumError + kd*rateError;                //PID output               

    lastError = error;                                //remember current error
    previousTime = currentTime;                        //remember current time
    //Serial.println(abs(out));
    return out;                                        //have function return the PID output
}

void FORWARD(int L, int R){

 
  analogWrite(left_forward, L);
  analogWrite(left_backward, no_speed);
  analogWrite(right_forward, R);
  analogWrite(right_backward, no_speed);

}

void STOP(){

  analogWrite(left_forward, no_speed);
  analogWrite(left_backward, no_speed);
  analogWrite(right_forward, no_speed);
  analogWrite(right_backward, no_speed);
/*digitalWrite( MotorL1, LOW );
digitalWrite(MotorL2, LOW);
digitalWrite(MotorR1, LOW);
digitalWrite( MotorR2, LOW);

analogWrite(Enable_L, 0);
analogWrite(Enable_R, 0);*/
}

void RIGHT(){
  analogWrite(left_forward, no_speed);
  analogWrite(left_backward, 90);
  analogWrite(right_forward, 230);
  analogWrite(right_backward, no_speed);
  
/*digitalWrite( MotorL1, HIGH );
digitalWrite(MotorL2, LOW);
digitalWrite(MotorR1, LOW);
digitalWrite( MotorR2, HIGH);

analogWrite(Enable_L, 90);
analogWrite(Enable_R, 255);*/
}

void LEFT(){
  analogWrite(left_forward, 230);
  analogWrite(left_backward, no_speed);
  analogWrite(right_forward, no_speed);
  analogWrite(right_backward, 90);
/*digitalWrite( MotorL1, HIGH );
digitalWrite(MotorL2, LOW);
digitalWrite(MotorR1, LOW);
digitalWrite( MotorR2, HIGH);

analogWrite(Enable_L, 255);
analogWrite(Enable_R,90);*/
}

void SLOW(){

  analogWrite(left_forward, 125);
  analogWrite(left_backward, no_speed);
  analogWrite(right_forward, 125);
  analogWrite(right_backward, no_speed);
  
/*digitalWrite( MotorL1, HIGH );
digitalWrite(MotorL2, LOW);
digitalWrite(MotorR1, LOW);
digitalWrite( MotorR2, HIGH);

analogWrite(Enable_L, 125);
analogWrite(Enable_R, 125);*/
}

void BACKWARD(int L, int R){

  analogWrite(left_forward, no_speed);
  analogWrite(left_backward, L);
  analogWrite(right_forward, no_speed);
  analogWrite(right_backward, R);
/*digitalWrite( MotorL1, LOW );
digitalWrite(MotorL2, HIGH);
digitalWrite(MotorR1, HIGH);
digitalWrite( MotorR2, LOW);

analogWrite(Enable_L, L);
analogWrite(Enable_R, R);*/
}

void SLOWBACKWARD(){
  analogWrite(left_forward, no_speed);
  analogWrite(left_backward, 125);
  analogWrite(right_forward, no_speed);
  analogWrite(right_backward, 125);
/*digitalWrite( MotorL1, LOW );
digitalWrite(MotorL2, HIGH);
digitalWrite(MotorR1, HIGH);
digitalWrite( MotorR2, LOW);

analogWrite(Enable_L, 125);
analogWrite(Enable_R, 125);*/
}

void LEFTBACKWARD(){

    analogWrite(left_forward, no_speed);
    analogWrite(left_backward, 190);
    analogWrite(right_forward, no_speed);
    analogWrite(right_backward, 100);
/*digitalWrite( MotorL1, LOW );
digitalWrite(MotorL2, HIGH);
digitalWrite(MotorR1, HIGH);
digitalWrite( MotorR2, LOW);

analogWrite(Enable_L, 190);
analogWrite(Enable_R, 125);*/
}

void RIGHTBACKWARD(){
    analogWrite(left_forward, no_speed);
    analogWrite(left_backward, 100);
    analogWrite(right_forward, no_speed);
    analogWrite(right_backward, 220);
/*digitalWrite( MotorL1, LOW );
digitalWrite(MotorL2, HIGH);
digitalWrite(MotorR1, HIGH);
digitalWrite( MotorR2, LOW);

analogWrite(Enable_L, 125);
analogWrite(Enable_R, 190);*/
}

void p180TURN(){
  analogWrite(left_forward, 200);
  analogWrite(left_backward, no_speed);
  analogWrite(right_forward, no_speed);
  analogWrite(right_backward, 200);

//analogWrite(Enable_L, 200);
//analogWrite(Enable_R, 200);

}


void distance_output(int x)
{
 
  switch(x){
    case(11):
        server.println('1');
        Serial.println('1');
        break;
    case(12):
        server.println('2');
        Serial.println('2');
        break;
    case(13):
        server.println('3');
        Serial.println('3');
        break;
    case(14):
        server.println('4');
        Serial.println('4');
        break;
    case(15):
        server.println('5');
        Serial.println('5');
        break;
    case(16):
        server.println('6');
        Serial.println('6');
        break;
    case(17):
        server.println('7');
        Serial.println('7');
        break;
    case(18):
        server.println('8');
        Serial.println('8');
        break;
    case(19):
        server.println('9');
        Serial.println('9');
        break;
  }
}

void drive(int d, double s)//testing to see if taking in the variable causes this function to print on processing faster
{
 int  right_sensor_state = digitalRead(REYE);
 int  left_sensor_state = digitalRead(LEYE);
 const int black = 1;
 const int white = 0;
 int  Speed = 125 +125*abs(s);
 
 
 if (Speed > 230)
 {
  Speed = 230;
 }
 if(right_sensor_state == black && left_sensor_state == black){
  Speed = 125;
 }
 //Serial.println(Speed);
 //server.write(Speed);
 
 
 //server.write(Speed);
 //Serial.println(Speed);
 //delay(1000);
 if(d > 16 && d != 0)
 {
  server.write('f');
  
 if(right_sensor_state != black && left_sensor_state != black)
  {
    FORWARD(Speed, Speed);
  }
   if(right_sensor_state != black && left_sensor_state == black)
   {
    RIGHT();
    }
   
  if(right_sensor_state == black && left_sensor_state != black)
  {
   LEFT();
  }
  
  if(right_sensor_state == black && left_sensor_state == black)
  {
   SLOW();//going back on track
  }
  if( d < 20 && d > 16){
        distance_output(d);
    }
}else if(d < 14 && d > 0)//speeding up reverse turn speeds may help cornering//
 {
  server.write('b');
    
    if( d < 14 && d > 10){
        distance_output(d);
    }
     
     
   if(right_sensor_state != black && left_sensor_state != black)
   {
      BACKWARD(Speed, Speed);
   }
   
    if(right_sensor_state != black && left_sensor_state == black)
    {
      RIGHTBACKWARD();
    }
    
  
    if(right_sensor_state == black && left_sensor_state != black)
    {
     LEFTBACKWARD();
    }
  
    if(right_sensor_state == black && left_sensor_state == black)
    {
     SLOWBACKWARD();
    }
 }if( d > 14 && d < 16)
   {
    STOP();
    distance_output(d);
   }
}

//interupt function
void ISR(){
 int x = 100000;
}

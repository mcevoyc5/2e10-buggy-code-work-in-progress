import processing.net.*;
import controlP5.*;
import meter.*;
//gui object
ControlP5 cp5;
//server objects
Client myClient;//motor contol
Client myClient2; //obstacle detection
Client myClient3; //voltmeter input

Knob ControlKnob;//starts and stops buggy

Textarea myTextarea;//output location for obstacle
Println console;//makes the text box the console

Meter m;///meter object

PFont font;//font objects
PFont controlFont;
PFont controlFont2;
PFont controlFont3;

void setup() {
  
  size(400,600);
  smooth();
  noStroke();
  
  //initialising GUI
  cp5 = new ControlP5(this);
  
  //initialising fonts
  font = createFont("calibri light", 30);
  controlFont = createFont("calibri light", 20);
  controlFont2 = createFont("calibri light", 16);
  controlFont3 = createFont("calibri light", 12);
  
  //initialising the clients
  myClient = new Client(this, "192.168.1.16", 80);//
  myClient2 = new Client(this, "192.168.1.16", 01);
  myClient3 = new Client(this, "192.168.1.16", 02);
 //creating knob
 ControlKnob = cp5.addKnob("DriveControl")
               .setRange(0,255)
               .setValue(0)
               .setColorForeground(color(200,0,0))
               .setColorBackground(color(255,0,0))
               .setColorActive(color(0,225,0))
               .setPosition(100,100)
               .setRadius(100)
               .setDragDirection(Knob.HORIZONTAL)
               .setFont(controlFont)
               ;
 //creating text area      
 myTextarea = cp5.addTextarea("Processing Output")
                .setPosition(143,353)
                .setSize(110,25)
                .setFont(createFont("arial",12))
                .setLineHeight(10)
                .setColorBackground(color(255,255,255))
                .setColor(color(0))
                ;
//initalising meter
m = new Meter(this, 50, 400);
//creating meter
  m.setTitle("Velocity");
  m.setMeterWidth(300);
  m.setScaleFontName("Arial bold");
  m.setScaleFontColor(color(200,30,70));
  m.setNeedleThickness(5);
  m.setDisplayMaximumNeedle(true);
  String[] scaleLabels = {"0", "50", "100", "150", "200", "250"};
  m.setScaleLabels(scaleLabels);
 
  //turn 180 degree contorl button
  cp5.addButton("TURN_180")
     //.setValue('X')
     .setPosition(20, 180)
     .setSize(60,30)
     ;
  //giving the text area the consol output   
  console = cp5.addConsole(myTextarea);   
  background(51);

}

void draw() {
  font = createFont("calibri light", 30); 
  //main title
   textFont(font);
   fill(color(255,255,255));
   text("2E10: Group Y07", 95, 45);
   text("Arduino Buggy Controls", 55, 75);
  //meter title
   fill(color(255,255,255));
   textFont(controlFont2);
   text("Velocity (% of Max)", 135, 395);
   //console box title
   fill(color(255,255,255));
   textFont(controlFont2);
   text("Obstacle Detected: ", 138, 345);
   
   //showing the pid values
   fill(color(255,255,255));
   textFont(controlFont2);
   text("kp = 1 / 7.3", 300, 245);
   text("ki = 1 / 20" , 300, 265);
   text("kd = 2" , 300, 285);
   
   //meter
   if(myClient3.active())//checks for correct server
   {
      int volts = myClient3.read();
      m.updateMeter(volts);  
   }
   //obstabcle
   if (myClient2.active())//checks for correct server
   {
      char data = myClient2.readChar();
    switch(data){
       case('1'):
         println("11 cm");
         break;
       case('2'):
         println("12 cm");
         break;
       case('3'):
         println("13 cm");
         break;
       case('4'):
         println("14 cm");
         break;
       case('5'):
         println("Stop: 15 cm");
         break;
       case('6'):
         println("16 cm");
         break;
       case('7'):
         println("17 cm");
         break;
       case('8'):
         println("18 cm");
         break;
       case('9'):
         println("19 cm");
         break;
    }
    
 }
}
//communicates with buggy via motor control server
void DriveControl(int Value)
{
  if (myClient.active())
  {
   myClient.write(Value);//sends this value to the server
  }
}

void TURN_180()
{
  if(myClient.active())
  {
    myClient.write('X');
    println('X');
  }
}
 

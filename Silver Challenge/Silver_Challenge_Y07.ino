#include <WiFiNINA.h>//connect server/client

char ssid[] = "xxxx";
char pass[] = "xxxx";

//different servers for reporting of data for improved speed
WiFiServer server(80);//motor contol server
WiFiServer server1(01);//obstacle server
WiFiServer server2(02);//voltmeter server
WiFiServer server3(03);//arrow server

int no_speed = 0;

//motor pins
const int MotorL1 = 5;
const int MotorL2= 6;
const int MotorR1 = 12;
const int MotorR2 = 2;


//IR Control
const int LED_PIN = 13;
const int LEYE = 3;
const int REYE = 11;

//speed pins
const int Enable_L = 17;
const int Enable_R = 16;

//ultrasonic
const int trigPin = 10;
const int echoPin = 9;

//ultrasonic eqn
float duration;
int distance;//convenience for outputting

//PID computed via tuning
double kp = (1/7.3);
double ki = 1/20;
double kd = 2;


//PID variables
unsigned long currentTime, previousTime;
double elapsedTime;
double error;
double lastError;
const double setPoint = 15;
double cumError, rateError;
double Speedvalue;

//outputting to GUI //do these do anything??
char state;
char prevState = 'z';

bool sentinal;

void setup() {
  Serial.begin(9600);
  
  //server/client connection
  WiFi.begin(ssid, pass);
  IPAddress ip = WiFi.localIP();
  
  //confirm connection
  Serial.print("IP Address:");
  Serial.println(ip);// prints when ip is connected
  server.begin();//starts motor control server
 server1.begin();//starts obstacle server
 server2.begin();//starts voltmeter server
 
  //set-up ultra
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  //set up motor pins
  pinMode(MotorL1, OUTPUT );
  pinMode( MotorL2, OUTPUT );
  pinMode( MotorR1, OUTPUT );
  pinMode(MotorR2, OUTPUT );
 
  //ir set-up
  pinMode( REYE, INPUT);
  pinMode( LEYE, INPUT);
  pinMode (LED_PIN, OUTPUT);//sort of superfluous

}

void loop() {//trying high low speeds to make pins work
  //wifi object
  WiFiClient client = server.available();
  WiFiClient client2 = server1.available();
  sentinal = 0;//repeating turn function boolean
  //checking connection
  if (client.connected()){
   Serial.println("Client Connected");
    int c = 1;// = client.read();
    
    char turn = client.read();//for tun function control
    
    while(c != 0 && turn != 'X'){
      c = client.read();//controls on off

      digitalWrite(trigPin, HIGH);//out signal
      delayMicroseconds(10);//delay between signals out
      digitalWrite(trigPin, LOW);//return signal
      
      duration = pulseIn(echoPin, HIGH);
      distance = (duration*.0293)/2;
      
      Speedvalue = computePID(distance);//for compoting speed variable based on pid and distance from object
      drive(distance, Speedvalue);//actual speed value is computed in drive function
    }
     
        if(turn == 'X' )
    {
      while (sentinal != 1)
      {
        //turning function for going opposite direction on track
        p180TURN();//causes turn
        delay(1200);
        STOP();
        sentinal = 1; 
      }
     }
      
  }
     
  STOP();
}
//PID Functions
double computePID(double inp){     
    currentTime = millis();  //get current time
    elapsedTime = (double)(currentTime - previousTime); //compute time elapsed from previous computation
    
    error = (setPoint - inp);// determine error
    cumError += error * elapsedTime;// compute integral
    rateError = (error - lastError)/elapsedTime;// compute derivative

    double out = kp*error + ki*cumError + kd*rateError;//PID output               

    lastError = error;//remember current error
    previousTime = currentTime; //remember current time
  
    return out;//have function return the PID Result
}
//functions for drive control
void FORWARD(int L, int R){

  digitalWrite( MotorL1, HIGH );
  digitalWrite(MotorL2, LOW);
  digitalWrite(MotorR1, LOW);
  digitalWrite( MotorR2, HIGH);
  
  analogWrite(Enable_L, L);
  analogWrite(Enable_R, R);

}

void STOP(){
  
  digitalWrite( MotorL1, LOW );
  digitalWrite(MotorL2, LOW);
  digitalWrite(MotorR1, LOW);
  digitalWrite( MotorR2, LOW);
  
  analogWrite(Enable_L, 0);
  analogWrite(Enable_R, 0);
  
}

void RIGHT(){
  
  digitalWrite( MotorL1, HIGH );
  digitalWrite(MotorL2, LOW);
  digitalWrite(MotorR1, LOW);
  digitalWrite( MotorR2, HIGH);
  
  analogWrite(Enable_L, 90);
  analogWrite(Enable_R, 255);
  
}

void LEFT(){

  digitalWrite( MotorL1, HIGH );
  digitalWrite(MotorL2, LOW);
  digitalWrite(MotorR1, LOW);
  digitalWrite( MotorR2, HIGH);
  
  analogWrite(Enable_L, 255);
  analogWrite(Enable_R,90);

}

void SLOW(){
  
  digitalWrite( MotorL1, HIGH );
  digitalWrite(MotorL2, LOW);
  digitalWrite(MotorR1, LOW);
  digitalWrite( MotorR2, HIGH);
  
  analogWrite(Enable_L, 125);
  analogWrite(Enable_R, 125);

}

void BACKWARD(int L, int R){


  digitalWrite( MotorL1, LOW );
  digitalWrite(MotorL2, HIGH);
  digitalWrite(MotorR1, HIGH);
  digitalWrite( MotorR2, LOW);
  
  analogWrite(Enable_L, L);
  analogWrite(Enable_R, R);
  
}

void SLOWBACKWARD(){
  
  digitalWrite( MotorL1, LOW );
  digitalWrite(MotorL2, HIGH);
  digitalWrite(MotorR1, HIGH);
  digitalWrite( MotorR2, LOW);
  
  analogWrite(Enable_L, 125);
  analogWrite(Enable_R, 125);
}

void LEFTBACKWARD(){


  digitalWrite( MotorL1, LOW );
  digitalWrite(MotorL2, HIGH);
  digitalWrite(MotorR1, HIGH);
  digitalWrite( MotorR2, LOW);
  
  analogWrite(Enable_L, 190);
  analogWrite(Enable_R, 125);
  
}

void RIGHTBACKWARD(){

  digitalWrite( MotorL1, LOW );
  digitalWrite(MotorL2, HIGH);
  digitalWrite(MotorR1, HIGH);
  digitalWrite( MotorR2, LOW);
  
  analogWrite(Enable_L, 125);
  analogWrite(Enable_R, 190);

}
//from main
void p180TURN(){
 
  digitalWrite( MotorL1, HIGH );
  digitalWrite(MotorL2, LOW);
  digitalWrite(MotorR1, LOW);
  digitalWrite( MotorR2, HIGH);
  
  analogWrite(Enable_L, 200);
  analogWrite(Enable_R, 200);

}

//for reporting distance from object
//uses chars for improved reporting speed
void distance_output(int x)
{
 switch(x)
 {
  case(11):
    server1.println('1');
    break;
  case(12):
    server1.println('2');
    break;
  case(13):
    server1.println('3');
    break;
  case(14):
    server1.println('4');
    break;
  case(15):
    server1.println('5');
    break;
  case(16):
    server1.println('6');
    break;
  case(17):
    server1.println('7');
    break;
  case(18):
    server1.println('8');
    break;
  case(19):
    server1.println('8');
    break;
 }
}

void drive(int d, double s)//testing to see if taking in the variable causes this function to print on processing faster
{
  //convenience variables: readablity
 int  right_sensor_state = digitalRead(REYE);
 int  left_sensor_state = digitalRead(LEYE);
 const int black = 1;
 const int white = 0;
 int  Speed = 125 +125*abs(s);
 
 if (Speed > 230)
 {
  Speed = 230;
 }
 if(right_sensor_state == black && left_sensor_state == black){
  Speed = 125;//help refind track if gone off
 }
 server2.write(Speed);//for voltmeter output
 
 if(d > 15 && d != 0)
 {
    
   if(right_sensor_state != black && left_sensor_state != black)
    {
      FORWARD(Speed, Speed);//PID conrolled speeds
    }
     if(right_sensor_state != black && left_sensor_state == black)
     {
      RIGHT();
      }
     
    if(right_sensor_state == black && left_sensor_state != black)
    {
     LEFT();
    }
    
    if(right_sensor_state == black && left_sensor_state == black)
    {
     SLOW();//going back on track
    }
    if( d < 20 && d > 16){
          distance_output(d);//object reporting
      }
 }else if(d < 14 && d > 0)//speeding up reverse turn speeds may help cornering//
 {
    
   if( d < 14 && d > 10){
        distance_output(d);//object reporting
   }
   if(right_sensor_state != black && left_sensor_state != black)
   {
      BACKWARD(Speed, Speed);//PID controlled speeds
   }
   
   if(right_sensor_state != black && left_sensor_state == black)
   {
      RIGHTBACKWARD();
   }
   if(right_sensor_state == black && left_sensor_state != black)
   {
     LEFTBACKWARD();
   }
   if(right_sensor_state == black && left_sensor_state == black)
   {
     SLOWBACKWARD();
   }
 }
 
 if( d > 14 && d < 16)
   {
    STOP();
    distance_output(d);
    int c = 0;
    server2.write(c);//speed = 0 when stopped
   }
}

import controlP5.*;
import processing.net.*;
ControlP5 cp5;
Client myClient;
String data;

void setup() {
size(800,300);
cp5 = new ControlP5(this);
myClient=new Client(this,"192.168.1.24",80);

cp5.addButton("Start")
.setValue(0)
.setPosition(300, 91)
.setSize(200,19);

cp5.addButton("Left")
.setValue(0)
.setPosition(50,150)
.setSize(200,19);

cp5.addButton("Right")
.setValue(0)
.setPosition(550,150)
.setSize(200,19);

cp5.addButton("Reverse")
.setValue(0)
.setPosition(300,219)
.setSize(200,19);

cp5.addButton("Stop")
.setValue(0)
.setPosition(300,150)
.setSize(200,19);

}

void draw() {
}

public void Start() {
if (myClient.active()){
myClient.write("w");
println("F Button Pressed");
}
}

public void Reverse() {
if (myClient.active()){
myClient.write("s");
println("B Button Pressed");
}
}

public void Left() {
if (myClient.active()){
myClient.write("a");
println("L Button Pressed");
}
}

public void Right() {
if (myClient.active()){
myClient.write("d");
println("R Button Pressed");
}
}

public void Stop() {
if (myClient.active()){
myClient.write("O");
println("S Button Pressed");
}
}

const int LED_PIN = 13;
const int MotorL1 = 5;
const int MotorL2= 6;
const int MotorR1 = 12;
const int MotorR2 = 2;

const int LEYE = 3;
const int REYE= 11;
int L_condition = HIGH;
int R_condition = HIGH;


const int Enable_L = A3;
const int Enable_R = A2;


void setup() {
  // put your setup code here, to run once:
Serial.begin( 9600 );
// motors
pinMode(MotorL1, OUTPUT );
pinMode( MotorL2, OUTPUT );
pinMode( MotorR1, OUTPUT );
pinMode(MotorR2, OUTPUT );

pinMode( REYE, INPUT);
pinMode( LEYE, INPUT);
pinMode ( LED_PIN, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
if (digitalRead(LEYE) != L_condition || digitalRead(REYE) != R_condition){
    Serial.println("Change Detected:");
    L_condition = digitalRead(LEYE);
    R_condition = digitalRead(REYE);
    
if( digitalRead( LEYE ) == HIGH && digitalRead( REYE )== HIGH ){
Serial.print("Heading Straight");
digitalWrite( MotorL1, HIGH );
digitalWrite(MotorL2, LOW); 
digitalWrite(MotorR1, LOW);
digitalWrite( MotorR2, HIGH);

analogWrite(Enable_L, 255);
analogWrite(Enable_R, 255);   
}



if( digitalRead( LEYE ) == LOW && digitalRead( REYE )== HIGH ){
Serial.print("Going Left");
digitalWrite( MotorL1, HIGH );
digitalWrite(MotorL2, LOW);
digitalWrite(MotorR1, LOW);
digitalWrite( MotorR2, HIGH);

analogWrite(Enable_L, 255);
analogWrite(Enable_R, 125);  
  }


if( digitalRead( LEYE ) == HIGH && digitalRead( REYE )== LOW ){
Serial.print("Going Right");
digitalWrite( MotorL1, HIGH );
digitalWrite(MotorL2, LOW);
digitalWrite(MotorR1, LOW);
digitalWrite( MotorR2, HIGH);

analogWrite(Enable_L, 125);
analogWrite(Enable_R, 255);  
}
 
if( digitalRead( LEYE ) == LOW && digitalRead( REYE )== LOW ){
Serial.print("Stopping");
digitalWrite( MotorL1, LOW );
digitalWrite(MotorL2, LOW);
digitalWrite(MotorR1, LOW);
digitalWrite( MotorR2, LOW);

analogWrite(Enable_L, 0);

analogWrite(Enable_R, 0);   
}
}
}

// Just checking out the checking out method with git and seeing how to revert to older versions dont mind this

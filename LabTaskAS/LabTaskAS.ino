#include <WiFiNINA.h>
char ssid[] = "VODAFONE-17F0";
char pass[] = "7L4CSUEUZUB8ZTM3";
WiFiServer server(80);
const int MotorL1 = 5;
const int MotorL2= 6;
const int MotorR1 = 12;
const int MotorR2 = 2;

const int Enable_L = A3;
const int Enable_R = A2;

void setup() {
Serial.begin(9600);
pinMode(MotorL1, OUTPUT );
pinMode( MotorL2, OUTPUT );
pinMode( MotorR1, OUTPUT );
pinMode(MotorR2, OUTPUT );
WiFi.begin(ssid, pass);
IPAddress ip = WiFi.localIP();
Serial.print("IP Address:");
Serial.println(ip);
server.begin();
}

void loop() {
WiFiClient client = server.available();
if (client.connected()) {
Serial.println("Client Connected");
char c = client.read();

if(c == 'w'){
Serial.println("Starting Motors");
start_r(); 
}
if(c =='s'){
Serial.println("Reversing Motors");
reverse_r();  
}
if(c =='a'){
Serial.println("Turning Left");
left_r();  
}
if(c =='d'){
Serial.println("Turning Right");
right_r();  
}

if(c =='O'){
Serial.println("Stopping Motors");
stop_r();  
}
}

}

void start_r(){
digitalWrite( MotorL1, HIGH );
digitalWrite(MotorL2, LOW);
digitalWrite(MotorR1, LOW);
digitalWrite( MotorR2, HIGH);

analogWrite(Enable_L, 190);
analogWrite(Enable_R, 190);
}
void reverse_r(){
digitalWrite( MotorL1, LOW );
digitalWrite(MotorL2, HIGH);
digitalWrite(MotorR1, HIGH);
digitalWrite( MotorR2, LOW);

analogWrite(Enable_L, 190);
analogWrite(Enable_R, 190);
}
void right_r(){
digitalWrite( MotorL1, HIGH );
digitalWrite(MotorL2, LOW);
digitalWrite(MotorR1, LOW);
digitalWrite( MotorR2, HIGH);

analogWrite(Enable_L, 125);
analogWrite(Enable_R, 255);
}
void left_r(){
digitalWrite( MotorL1, HIGH );
digitalWrite(MotorL2, LOW);
digitalWrite(MotorR1, LOW);
digitalWrite( MotorR2, HIGH);

analogWrite(Enable_L, 255);
analogWrite(Enable_R,125);
}

void stop_r(){
digitalWrite( MotorL1, LOW );
digitalWrite(MotorL2, LOW);
digitalWrite(MotorR1, LOW);
digitalWrite( MotorR2, LOW);

analogWrite(Enable_L, 0);
analogWrite(Enable_R, 0);
}

import processing.net.*;
import controlP5.*;

ControlP5 cp5;
Client myClient;
Client myClient2; 
Client myClient3;
Knob ControlKnob;

int angle = 0;
int pitch = 460;

PFont font;//font object
PFont font2;
PFont controlFont;
PFont controlFont2;
PFont controlFont3;
void setup() {
  size(700,600);
  smooth();
  noStroke();
  //background(51);
  
  cp5 = new ControlP5(this);
  font = createFont("calibri light", 30);
  controlFont = createFont("calibri light", 20);
  controlFont2 = createFont("calibri light", 16);
  controlFont3 = createFont("calibri light", 12);
  myClient = new Client(this, "192.168.1.25", 80);//
  myClient2 = new Client(this, "192.168.1.25", 01);
  myClient3 = new Client(this, "192.168.1.25", 03);
  
  
 ControlKnob = cp5.addKnob("DriveControl")
               .setRange(0,255)
               .setValue(0)
               .setColorForeground(color(200,0,0))
               .setColorBackground(color(255,0,0))
               .setColorActive(color(0,225,0))
               .setPosition(50,200)
               .setRadius(100)
               .setDragDirection(Knob.HORIZONTAL)
               .setFont(controlFont)
               ;
               
  background(51);

}

void draw() {
  font = createFont("calibri light", 30); 
  font2 = createFont("calibri light", 20);
   
   textFont(font);
   fill(color(255,255,255));
   text("2E10: Group Y07", 240, 45);
   text("Arduino Buggy Controls & Sensors", 140, 75);
   
   fill(51, 51, 51);
   stroke(255, 0, 0);
   strokeWeight(3);
   circle(500, 300, 250);
   fill(255, 0, 0);
   stroke(255, 0, 0);
   strokeWeight(3);
   circle(500, 300, 10);
   
   fill(color(255,255,255));
   text(" 0 ", 491, 162);
   text("180", 483, 448);
   text("-90 " , 630, 305);
   text(" 90 " , 340, 305);
   
   fill(0, 102, 153);
   strokeWeight(2);
   stroke(255,0,0);
  
   if (myClient.active()){
    int data = myClient.read();
    //println(data);
    int degree = data;
    DrawNeedle(degree);
   }
   
 if (myClient.active()){
    int dataP = myClient.read();
    println(dataP);
 }
}
   
void DriveControl(int Value){
  if (myClient.active()){
      myClient.write(Value);
      if(Value == 0){
      }
  }
}

void DrawNeedle(int degree){
  pushMatrix();
  angle = -degree-90; //*(-45) - 90;
  translate(500, 300);
  rotate(radians(angle));
  line(0,0,110, 0);
  fill(255, 0, 0);
  triangle(90, -8, 90, 8, 120, 0);
  popMatrix();
}

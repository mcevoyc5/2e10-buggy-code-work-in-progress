#include <Arduino_LSM6DS3.h>
#include <WiFiNINA.h>//connect server/client

char ssid[] = "xxx";
char pass[] = "xxx";

WiFiServer server(80);
WiFiServer server2(20);
WiFiServer server3(30);
WiFiServer server4(03);//voltmeter
//motor pins
const int MotorL1 = 5;
const int MotorL2= 6;
const int MotorR1 = 12;
const int MotorR2 = 11;

//IR Control
const int LED_PIN = 13;
const int LEYE = 3;
const int REYE = 2;

//speed pins
const int Enable_L = 17;
const int Enable_R = 16;

//ultrasonic
const int trigPin = 10;
const int echoPin = 9;

//ultrasonic eqn
float duration;
int distance;//convenience for outputting
//PID
double kp = (1/7.3);
double ki = 1/20;
double kd = 2;
 
unsigned long CurrentTime, PreviousTime;
double ElapsedTime;
double error;
double lastError;
const double setPoint = 15;
double cumError, rateError;
double Speedvalue;

bool sentinal;

//gyroscope calc variables
float GyroX, GyroY, Gyro;
float yaw;
float GyroErrorX, GyroErrorY, GyroError;
float elapsedTime, currentTime, previousTime;
int Q = 0;

void setup() {
  Serial.begin(9600);
  
  //server/client connection
  WiFi.begin(ssid, pass);
  IPAddress ip = WiFi.localIP();
  
  //confirm connection
  Serial.print("IP Address:");
  Serial.println(ip);// prints when ip is connected
  server.begin();//starts the server
  server2.begin();
  server3.begin();
  //set-up ultra
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  //set up motor pins
  pinMode( MotorL1, OUTPUT );
  pinMode( MotorL2, OUTPUT );
  pinMode( MotorR1, OUTPUT );
  pinMode( MotorR2, OUTPUT );
  
  //ir set-up
  pinMode( REYE, INPUT);
  pinMode( LEYE, INPUT);
  pinMode (LED_PIN, OUTPUT);//sort of superfluous

  Serial.begin(9600);
  while (!Serial);

  if (!IMU.begin()) {
    Serial.println("Failed to initialize IMU!");

    while (1);
  }

  calculate_IMU_error();
  //function for printing distance
  attachInterrupt(digitalPinToInterrupt(echoPin), ISR, CHANGE);//for printing

}

void loop() {//trying high low speeds to make pins work
  //wifi object
  WiFiClient client = server.available();
  WiFiClient client2 = server2.available();
  WiFiClient client3 = server3.available();
  //checking connection
  if (client.connected()){
   Serial.println("Client Connected");
    int c = 1;// = client.read();
    char turn = client.read();
    while(c != 0){
     
    previousTime = currentTime;        
    currentTime = millis();         
    elapsedTime = (currentTime - previousTime) / 1000;
  
    IMU.readGyroscope(GyroX, GyroY, Gyro);//only z 'gyro' used for yaw

    
    Gyro = Gyro - GyroError; // GyroErrorZ 
  
    yaw =  yaw + (Gyro * (elapsedTime)*1.15);
  
    if(yaw >= 360 || yaw <= -360){
      yaw = 0;
    }
    int NeatYaw = yaw;
   
    print_degree(NeatYaw);
  
    Serial.println(NeatYaw);
    
      c = client.read();//controls on off
      digitalWrite(trigPin, HIGH);
      delayMicroseconds(10);
      digitalWrite(trigPin, LOW);
      
      duration = pulseIn(echoPin, HIGH);
      distance = (duration*.0293)/2;
      
      Speedvalue = computePID(distance);
      drive(distance, Speedvalue);//actual speed value is computed in drive functio
    }
  }
  STOP();
}
//error calculation for gyroscope
void calculate_IMU_error() {

  while (Q < 200) {
    
    IMU.readGyroscope(GyroX, GyroY, Gyro);
  
    // Sum all readings
    
    GyroError = GyroError + Gyro;//cumulative error
    Q++;
  }
  //divide sum by 200:average
  GyroError = GyroError / 200;
  Serial.println(GyroError);
}

//speed control via ultrsonic pid 
double computePID(double inp){     
    CurrentTime = millis();//get current time
    ElapsedTime = (double)(CurrentTime - PreviousTime); //compute time elapsed from previous computation
    
    error = (setPoint - inp); // determine error
    cumError += error * ElapsedTime; // compute integral
    rateError = (error - lastError)/ElapsedTime; // compute derivative

    double out = kp*error + ki*cumError + kd*rateError; //PID output               

    lastError = error;//remember current error
    PreviousTime = CurrentTime;//remember current time
  
    return out;//have function return the PID output
}
//drive functions
void FORWARD(int L, int R){

  digitalWrite( MotorL1, HIGH );
  digitalWrite(MotorL2, LOW);
  digitalWrite(MotorR1, LOW);
  digitalWrite( MotorR2, HIGH);

  analogWrite(Enable_L, L);
  analogWrite(Enable_R, R);
}

void STOP(){
  
  digitalWrite( MotorL1, LOW );
  digitalWrite(MotorL2, LOW);
  digitalWrite(MotorR1, LOW);
  digitalWrite( MotorR2, LOW);

  analogWrite(Enable_L, 0);
  analogWrite(Enable_R, 0);
}

void RIGHT(){
  
  digitalWrite( MotorL1, HIGH );
  digitalWrite(MotorL2, LOW);
  digitalWrite(MotorR1, LOW);
  digitalWrite( MotorR2, HIGH);

  analogWrite(Enable_L, 90);
  analogWrite(Enable_R, 255);
}

void LEFT(){

  digitalWrite( MotorL1, HIGH );
  digitalWrite(MotorL2, LOW);
  digitalWrite(MotorR1, LOW);
  digitalWrite( MotorR2, HIGH);

  analogWrite(Enable_L, 255);
  analogWrite(Enable_R,90);
}

void SLOW(){

  digitalWrite( MotorL1, HIGH );
  digitalWrite(MotorL2, LOW);
  digitalWrite(MotorR1, LOW);
  digitalWrite( MotorR2, HIGH);

  analogWrite(Enable_L, 125);
  analogWrite(Enable_R, 125);
}

void BACKWARD(int L, int R){

  digitalWrite( MotorL1, LOW );
  digitalWrite(MotorL2, HIGH);
  digitalWrite(MotorR1, HIGH);
  digitalWrite( MotorR2, LOW);

  analogWrite(Enable_L, L);
  analogWrite(Enable_R, R);
}

void SLOWBACKWARD(){

  digitalWrite( MotorL1, LOW );
  digitalWrite(MotorL2, HIGH);
  digitalWrite(MotorR1, HIGH);
  digitalWrite( MotorR2, LOW);

  analogWrite(Enable_L, 125);
  analogWrite(Enable_R, 125);
}

void LEFTBACKWARD(){

  digitalWrite( MotorL1, LOW );
  digitalWrite(MotorL2, HIGH);
  digitalWrite(MotorR1, HIGH);
  digitalWrite( MotorR2, LOW);

  analogWrite(Enable_L, 190);
  analogWrite(Enable_R, 125);
}

void RIGHTBACKWARD(){
  
  digitalWrite( MotorL1, LOW );
  digitalWrite(MotorL2, HIGH);
  digitalWrite(MotorR1, HIGH);
  digitalWrite( MotorR2, LOW);

  analogWrite(Enable_L, 125);
  analogWrite(Enable_R, 190);
}

void drive(int d, double s){
  int  right_sensor_state = digitalRead(REYE);
  int  left_sensor_state = digitalRead(LEYE);
  const int black = 1;
 const int white = 0;
 int  Speed = 125 +125*abs(s);
 
 if (Speed > 230){
  Speed = 230;
 }
 
 if(right_sensor_state == black && left_sensor_state == black){
  Speed = 125;
 }
 server4.write(Speed);//voltmeter in gui
 
if(d > 15 && d != 0){
  
 if(right_sensor_state != black && left_sensor_state != black){
    FORWARD(Speed, Speed);
 }
 
 if(right_sensor_state != black && left_sensor_state == black){
    RIGHT();
 }
   
 if(right_sensor_state == black && left_sensor_state != black){
   LEFT();
 }
  
 if(right_sensor_state == black && left_sensor_state == black){
   SLOW();//going back on track
 } 
}

else if(d < 15 && d > 0){
     
  if(right_sensor_state != black && left_sensor_state != black){
    BACKWARD(Speed, Speed);
  }
   
  if(right_sensor_state != black && left_sensor_state == black){
    RIGHTBACKWARD();
  }
    
  if(right_sensor_state == black && left_sensor_state != black){
    LEFTBACKWARD();
  }
  
  if(right_sensor_state == black && left_sensor_state == black){
    SLOWBACKWARD();
  }
}

`else {
  STOP();
 }
}

//interupt function helps printing
void ISR(){
 int x = 100000;
}
void print_degree(int c) {
 
  if(c > 0 && c < 10)//sends to processing for obstacle detection output
    {
      server3.println('a'); 
      delay(100);
    }else if(c > 10 && c < 20){
       server3.println('b'); 
       delay(100);
    }else if( c > 20 && c < 30){
      server3.println('c'); 
      delay(100);
    }else if(c > 30 && c < 40){
      server3.println('d'); 
        delay(100);
    }else if(c > 40 && c < 50){
      server3.println('e'); 
        delay(100);
    }else if(c > 50 && c < 60){
      server3.println('f'); 
        delay(100);
    }else if(c > 60 && c < 70){
      server3.println('h'); 
        delay(100);
    }else if(c > 70 && c < 80){
      server3.println('i'); 
        delay(100);
    }else if(c > 80 && c < 90){
      server3.println('j'); 
        delay(100);
    }
     else if(c < 0 && c > -10){
      server3.println('k'); 
        delay(100);
    }
  else if(c < -10 && c > -20){
      server3.println('l'); 
        delay(100);
    }
else if(c < -20 && c > -30){
      server3.println('m'); 
        delay(100);
    }
    else if(c < -30 && c > -40){
      server3.println('n'); 
        delay(100);
    }
    else if(c < -40 && c > -50){
      server3.println('o'); 
        delay(100);
    }
    else if(c < -50 && c > -60){
      server3.println('p'); 
        delay(100);
    }
    else if(c < -60 && c > -70){
      server3.println('q'); 
        delay(100);
    }
    else if(c < -70 && c > -80){
      server3.println('r'); 
        delay(100);
    }
     else if(c < -80 && c > -90){
      server3.println('s'); 
        delay(100);
    }
}

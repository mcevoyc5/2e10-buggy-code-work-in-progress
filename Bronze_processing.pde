import processing.net.*;
import controlP5.*;

ControlP5 cp5;
Client myClient;

Knob ControlKnob;

Textarea myTextarea;
Println console;

PFont font;//font object

void setup() {
  size(400,600);
  smooth();
  noStroke();
  //background(51);
  
  cp5 = new ControlP5(this);
  font = createFont("calibri light", 30);
  myClient = new Client(this, "192.168.1.25", 80);//
  
 ControlKnob = cp5.addKnob("DriveControl")
               .setRange(0,255)
               .setValue(0)
               .setColorForeground(color(200,0,0))
               .setColorBackground(color(255,0,0))
               .setColorActive(color(0,225,0))
               .setPosition(100,100)
               .setRadius(100)
               .setDragDirection(Knob.HORIZONTAL)
               .setFont(font)
               ;
               
 myTextarea = cp5.addTextarea("Processing Output")
                .setPosition(50,350)
                .setSize(300,220)
                .setFont(createFont("arial",12))
                .setLineHeight(14)
                .setColorBackground(color(255,255,255))
                .setColor(color(122))
                ;

  console = cp5.addConsole(myTextarea);   
  background(51);
}

void draw() {
  font = createFont("calibri light", 30); 
  
  background(0,0,0);
  
  text("2E10: Group Y07", 95, 45);
  text("Arduino Buggy Controls", 55, 75);
  
  textFont(font);
  
  if (myClient.active()){
  char data = myClient.readChar();
        if (data == 'a'){
          print("object detected: ");
        }else if(data == '1'){
          println("1 cm");
        }else if(data == '2'){
          println("2 cm");
        }else if(data == '3'){
          println("3 cm");
        }else if(data == '4'){
          println("4 cm");
        }else if(data == '5'){
          println("5 cm");
        }else if(data == '6'){
          println("6 cm");
        }else if(data == '7'){
          println("7 cm");
        }else if(data == '8'){
          println("8 cm");
        }else if(data == '9'){
          println("9 cm");
        }
   }
}

void DriveControl(int Value) {
  if (myClient.active()){
      myClient.write(Value);
      //char message = myClient.readChar();
       //if (message == 'a'){
       //    println("object detected");
      // }
  }
}

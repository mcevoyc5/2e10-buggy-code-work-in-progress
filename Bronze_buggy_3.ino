#include <WiFiNINA.h>//connect server/client

char ssid[] = "eircom04275807";//wifi info
char pass[] = "e8bd6eaa185f";
WiFiServer server(80);//server address
int high = 230;
int forward_speed = 180;
int no_speed = 0;

//motor pins
const int right_forward = 5;//L1
const int right_backward= 6;//L2
const int left_backward = 12;//R1
const int left_forward = 2;//r2

//IR Control
const int LED_PIN = 13;
const int LEYE = 3;
const int REYE = 11;
//speed pins: stopping my wifi module
//const int Enable_L = 17;
//const int Enable_R = 16;

//ultrasonic
const int trigPin = 10;
const int echoPin = 9;

//ultrasonic eqn
float duration;
int distance;//convenience for outputting

void setup() {
  
  Serial.begin(9600);
  
  //server/client connection
  WiFi.begin(ssid, pass);
  IPAddress ip = WiFi.localIP();
  
  //confirm connection
  Serial.print("IP Address:");
  Serial.println(ip);// prints when ip is connected
  server.begin();//starts the server
  
  //set-up ultra
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  //set up motor pins
  pinMode(left_forward, OUTPUT );
  pinMode( left_backward, OUTPUT );
  pinMode( right_forward, OUTPUT );
  pinMode(right_backward, OUTPUT );
  //ir set-up
  pinMode( REYE, INPUT);
  pinMode( LEYE, INPUT);
  pinMode (LED_PIN, OUTPUT);//sort of superfluous
}

void loop() {//trying high low speeds to make pins work
  //wifi object
  WiFiClient client = server.available();
  //checking connection
  if (client.connected()) 
  {
    int c = 1;
    while(c != 0)
    {
     c = client.read();//controls on off
     server.write(1);//find out what this does
      
      digitalWrite(trigPin, HIGH);
      delayMicroseconds(10);
      digitalWrite(trigPin, LOW);
      
      duration = pulseIn(echoPin, HIGH);
      distance = (duration*.0293)/2;
      
        drive(distance);
    }
  }    
    STOP();
}


void FORWARD()
{
    analogWrite(left_forward, forward_speed);
    analogWrite(left_backward, no_speed);
    analogWrite(right_forward, forward_speed);
    analogWrite(right_backward, no_speed);
}

void STOP()
{
    analogWrite(left_forward, no_speed);
    analogWrite(left_backward, no_speed);
    analogWrite(right_forward, no_speed);
    analogWrite(right_backward, no_speed);
}

void RIGHT()
{
    analogWrite(left_forward, no_speed);
    analogWrite(left_backward, high);
    analogWrite(right_forward, high);
    analogWrite(right_backward, no_speed);
}

void LEFT()
{
    analogWrite(left_forward, high);
    analogWrite(left_backward, no_speed);
    analogWrite(right_forward, no_speed);
    analogWrite(right_backward, high);
}
void distance_output(int x)
{
  if(x == 1)//sends to processing for obstacle detection output
    {
      server.println('1');//signals for distance
    }else if(x == 2){
      server.println('2');
    }else if(x == 3){
      server.println('3');
    }else if(x == 4){
      server.println('4');
    }else if(x == 5){
      server.println('5');
    }else if(x == 6){
      server.println('6');
    }else if(x == 7){
      server.println('7');
    }else if(x == 8){
      server.println('8');
    }else if(x == 9){
      server.println('9');
    }
    delay(1000);
}

void drive(int d)//testing to see if taking in the variable causes this function to print on processing faster
{
 int  right_sensor_state = digitalRead(REYE);
 int  left_sensor_state = digitalRead(LEYE);
 const int black = 1;
 const int white = 0;
 if(right_sensor_state != black && left_sensor_state != black)
  {
    FORWARD();
    //both forward
  }
   if(right_sensor_state != black && left_sensor_state == black)
   {
    RIGHT();
    
    //left sensor black
    //right forward, left back
    }
   
  if(right_sensor_state == black && left_sensor_state != black)
  {
   LEFT();
   //right sensor black
   //left forward, right back
   
  } 
  if(d < 10 && d > 0){
    //stop if object seen
    STOP();
    server.println('a');
    distance_output(d);
  }
}
